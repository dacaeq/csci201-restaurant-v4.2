package restaurant;

import java.util.HashMap;
import java.util.Set;

/**
 * 
 * @author Clayton Ketner
 *
 */
public class Menu 
{
    private HashMap<String, Integer> prices;
    public HashMap<String, Integer> defaultPrices;

    @SuppressWarnings("unchecked")
	public Menu()
    {
    	defaultPrices = new HashMap<String, Integer>();
    	defaultPrices.put("Steak", 16);
    	defaultPrices.put("Chicken", 13);
    	defaultPrices.put("Pizza", 10);
    	defaultPrices.put("Salad", 6);
    	
    	this.prices = (HashMap<String, Integer>) defaultPrices.clone();
    }
    
    public Menu deepCopy()
    {
    	Menu duplicateMenu = new Menu();
    	duplicateMenu.prices = prices;
    	
    	return duplicateMenu;
    }
    
    public Set<String> getChoices()
    {
    	return prices.keySet();
    }

    public int getPriceOf(String choice)
    {
    		if (prices.containsKey(choice))
    			return prices.get(choice);

    	System.err.println("Tried to get price of " + choice + ", but menu doesn't contain that!");
    	return -1;
    }

    /**
     * For use by the host
     * @param choice
     */
    public void removeChoice(String choice)
    {
    	prices.remove(choice);
    }
    
    /**
     * For use by the host. Restores an item from the original menu.
     * @param choice
     * @param price
     */
    public void restoreChoice(String choice)
    {
    	prices.put(choice, defaultPrices.get(choice));
    }
    
    /**
     * Used to add an entirely new choice to the menu.
     * @param choice
     * @param price
     */
    public void addChoice(String choice, int price)
    {
    	prices.put(choice, (Integer)price);
    	
    	defaultPrices.put(choice, (Integer)price);
    }
}
    


















