package restaurant.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import restaurant.CashierAgent;
import restaurant.CustomerBill;
import restaurant.MarketBill;
import restaurant.Menu;
import restaurant.test.mock.*;

/**
 * 
 * @author Clayton Ketner
 *
 */
public class CashierAgentTest 
{
	private CashierAgent cashier; 
	private Menu menu;
	private MockWaiter waiter;
	private MockCustomer customer;
	private MockMarket market;

	@Before
	public void setUp() throws Exception 
	{
		menu = new Menu();
		cashier = new CashierAgent(menu);
		waiter = new MockWaiter("w1");
		customer = new MockCustomer("c1");
		market = new MockMarket("m1");
	}

	/**
	 * Tests if the cashier sends the appropriate bill to the waiter after the waiter has requested it
	 */
	@Test
	public void testMsgNeedBill()
	{
		// Send a request to the cashier for the bill
		cashier.msgNeedBill(waiter, customer, 1, "Steak");
		cashier.pickAndExecuteAnAction(); // Cashier will send bill to waiter
		
		assertTrue(waiter.log.getLastLoggedEvent().getMessage().equals(
					"Received msgBillReady for table 1 charging $16"));
	}

	/**
	 * Tests if a paying customer gets his receipt back with the appropriate change
	 */
	@Test
	public void testMsgHereIsBillFromCustomer()
	{
		// Test if a customer gets his receipt with appropriate change back
		CustomerBill bill = new CustomerBill(waiter, customer, 1);
		bill.addItem("Steak", 1);
		bill.calculate(menu.defaultPrices);
		bill.pay(20, 20);
		
		cashier.msgHereIsBill(bill);
		cashier.pickAndExecuteAnAction(); 
		// Cashier will check the bill and send msgThankYouForComing to customer
		
		assertTrue(customer.log.getLastLoggedEvent().getMessage().equals("Received msgThankYouForComing with $4"));
	}
	
	/**
	 * Tests if a customer who doesn't have enough to pay is told to wash dishes
	 */
	@Test
	public void testMsgHereIsBillFromPoorCustomer()
	{
		CustomerBill bill = new CustomerBill(waiter, customer, 1);
		bill.addItem("Steak", 1);
		bill.calculate(menu.defaultPrices);
		bill.pay(10, 10);
		
		cashier.msgHereIsBill(bill);
		cashier.pickAndExecuteAnAction(); 
		// Cashier will check the bill and send msgWashDishes to customer
		
		assertTrue(customer.log.getLastLoggedEvent().getMessage().contains("Received msgWashDishes"));
	}
	
	/**
	 * Tests if the request for a market bill is paid after being received
	 */
	@Test
	public void testMsgHereIsBillFromMarket()
	{
		MarketBill bill = new MarketBill(market);
		bill.addItem("Steak", 1);
		bill.addItem("Chicken", 2);
		bill.addItem("Pizza", 3);
		bill.addItem("Salad", 4);
		bill.calculate(menu.defaultPrices);
		
		cashier.msgHereIsBill(bill);
		cashier.pickAndExecuteAnAction();
		// Cashier will pay the bill and send it back to the market
		
		assertTrue(market.log.getLastLoggedEvent().getMessage().contains("Received msgHereIsPayment with $" 
																		  + bill.checkCharge()));
	}

}




























