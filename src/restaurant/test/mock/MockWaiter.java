package restaurant.test.mock;

import restaurant.CustomerBill;
import restaurant.Menu;
import restaurant.interfaces.Customer;
import restaurant.interfaces.Waiter;
import restaurant.layoutGUI.Food;

public class MockWaiter extends MockAgent implements Waiter 
{
	public EventLog log;
	
	public MockWaiter(String name)
	{
		super(name);
		log = new EventLog();
	}

	@Override
	public void msgBillReady(CustomerBill bill)
	{
		log.add(new LoggedEvent("Received msgBillReady for table "
								+ bill.table + " charging $" + bill.checkCharge()));
	}

	@Override
	public void msgOrderIsReady(int tableNum, Food food)
	{
		log.add(new LoggedEvent("Received msgOrderIsReady for table " + tableNum + " and food " + food.getName()));
	}

	@Override
	public void msgOutOfChoice(int tableNum, String choice)
	{
		log.add(new LoggedEvent("Received msgOutOfChoice for choice " + choice + " at table " + tableNum));
	}

	@Override
	public void msgImReadyToOrder(Customer customer)
	{
		log.add(new LoggedEvent("Received msgImReadyToOrder from " + customer.getName()));
	}

	@Override
	public void msgHereIsMyChoice(Customer customer, String choice)
	{
		log.add(new LoggedEvent("Received msgHereIsMyChoice from " + customer.getName() + " with choice " + choice));
	}

	@Override
	public void msgReadyForBill(Customer customer)
	{
		// TODO Auto-generated method stub
		log.add(new LoggedEvent(""));
	}

	@Override
	public void msgImLeaving(Customer customer)
	{
		// TODO Auto-generated method stub
		log.add(new LoggedEvent(""));
	}

	@Override
	public void msgOkToGoOnBreak()
	{
		// TODO Auto-generated method stub
		log.add(new LoggedEvent(""));
	}

	@Override
	public void msgCannotGoOnBreak()
	{
		// TODO Auto-generated method stub
		log.add(new LoggedEvent(""));
	}

	@Override
	public void msgSitCustomerAtTable(Customer customer, int tableNum)
	{
		// TODO Auto-generated method stub
		log.add(new LoggedEvent(""));
	}

	@Override
	public void msgMenuUpdated(Menu menu)
	{
		// TODO Auto-generated method stub
		log.add(new LoggedEvent(""));
	}

	@Override
	public boolean isOnBreak()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setBreakStatus(boolean selected)
	{
		// TODO Auto-generated method stub
		
	}

}
