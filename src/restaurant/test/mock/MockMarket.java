package restaurant.test.mock;

import java.util.ArrayList;

import restaurant.MarketBill;
import restaurant.MarketItem;
import restaurant.MarketOrder;
import restaurant.interfaces.Market;

public class MockMarket extends MockAgent implements Market
{
	public EventLog log;

	public MockMarket(String name)
	{
		super(name);
		log = new EventLog();
	}

	@Override
	public void msgHereIsPayment(MarketBill mbill)
	{
		log.add(new LoggedEvent("Received msgHereIsPayment with $" + mbill.getPayment()));
	}

	@Override
	public void msgOrderSupplies(MarketOrder mo)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<MarketItem> getInventory()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void changeQuantityOfItem(String item, int delta)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startThread()
	{
		// TODO Auto-generated method stub
		
	}

}
