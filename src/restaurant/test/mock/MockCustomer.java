package restaurant.test.mock;

import restaurant.CustomerBill;
import restaurant.Menu;
import restaurant.Receipt;
import restaurant.interfaces.Cashier;
import restaurant.interfaces.Customer;
import restaurant.interfaces.Host;
import restaurant.interfaces.Waiter;
import restaurant.layoutGUI.GuiCustomer;

public class MockCustomer extends MockAgent implements Customer
{
	public EventLog log;
	
	public MockCustomer(String name)
	{
		super(name);
		log = new EventLog();
	}

	@Override
	public void msgThereIsAWait()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void msgFollowMeToTable(Waiter waiterAgent, Menu menu)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void msgThankYouForComing(Receipt receipt)
	{
		log.add(new LoggedEvent("Received msgThankYouForComing with $" + receipt.takeChange()));
	}

	@Override
	public void msgWashDishes(int duration)
	{
		log.add(new LoggedEvent("Received msgWashDishes for " + duration));
	}

	@Override
	public void msgWhatWouldYouLike()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void msgHereIsYourFood(String choice)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void msgPleaseReorder(Menu menu)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void msgHereIsBill(CustomerBill bill)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public GuiCustomer getGuiCustomer()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isHungry()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getCash()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setCash(int amt)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setHungry()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setHost(Host host)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCashier(Cashier cashier)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startThread()
	{
		// TODO Auto-generated method stub
		
	}
}
