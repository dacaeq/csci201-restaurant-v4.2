package restaurant.test.mock;

import java.util.ArrayList;
import java.util.HashMap;

import restaurant.MarketItem;
import restaurant.MarketOrder;
import restaurant.OrderSheet;
import restaurant.interfaces.Cashier;
import restaurant.interfaces.Cook;
import restaurant.interfaces.Host;
import restaurant.interfaces.Market;
import restaurant.interfaces.Waiter;

/**
 * @author Sean Turner
 * 
 */
public class MockCook extends MockAgent implements Cook {

	public MockCook(String name) 
	{
		super(name);
	}

	public EventLog log = new EventLog();

	public void msgHereIsAnOrder(Waiter waiter, int tableNum, String choice) {
		log.add(new LoggedEvent(
				"Received message msgHereIsAnOrder from waiter "
						+ waiter.toString() + " for table number " + tableNum
						+ " to cook item " + choice + "."));

	}

	@Override
	public void msgHereIsAnOrder(OrderSheet orderSheet) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void msgNewOrder() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void msgWillSend(MarketOrder m, int shippingTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void msgSuppliesShipped(ArrayList<MarketItem> shipmentManifest,
			MarketOrder m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HashMap<String, Integer> getInventory() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void changeQuantityOfItem(String item, int delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addMarket(Market newMarket) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startThread() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCashier(Cashier cashier) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setHost(Host host) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

}