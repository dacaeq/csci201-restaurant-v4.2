package restaurant;

import agent.Agent;
import java.util.*;

import restaurant.interfaces.Customer;
import restaurant.interfaces.Host;
import restaurant.interfaces.Waiter;


/** Host agent for restaurant.
 *  Keeps a list of all the waiters and tables.
 *  Assigns new customers to waiters for seating and 
 *  keeps a list of waiting customers.
 *  Interacts with customers and waiters.
 */
public class HostAgent extends Agent implements Host
{

	/** Private class storing all the information for each table,
	 * including table number and state. */
	private class Table {
//		public int tableNum;
		public boolean occupied;

		/** Constructor for table class.
		 * @param num identification number
		 */
		public Table(int num){
//			tableNum = num;
			occupied = false;
		}	
	}

	/** Private class to hold waiter information and state */
	private class MyWaiter {
		public Waiter wtr;
		public boolean working = true;
		public boolean wantsBreak = false;

		/** Constructor for MyWaiter class
		 * @param waiter
		 */
		public MyWaiter(Waiter waiter){
			wtr = waiter;
		}
	}

	//List of all the customers that need a table
	private List<HostCustomer> waitList = Collections.synchronizedList(new ArrayList<HostCustomer>());

	//List of all waiter that exist.
	private List<MyWaiter> waiters =
			Collections.synchronizedList(new ArrayList<MyWaiter>());
	private int nextWaiter = 0; //The next waiter that needs a customer

	//List of all the tables
	int nTables;
	private Table tables[];

	//Name of the host
	private String name;
	
	private Menu menu;
	private boolean menuUpdated = false;

	/** Constructor for HostAgent class 
	 * @param name name of the host */
	public HostAgent(String name, int ntables) {
		super();
		this.nTables = ntables;
		tables = new Table[nTables];

		for(int i=0; i < nTables; i++){
			tables[i] = new Table(i);
		}
		this.name = name;
		
		menu = new Menu();
	}
	
	private class HostCustomer
	{
		Customer c;
		boolean hasBeenAskedToWait;
		
		public HostCustomer(Customer c)
		{
			this.c = c;
			hasBeenAskedToWait = false;
		}
	}

	//BOOKMARK
	// *** MESSAGES ***

	/** Customer sends this message to be added to the wait list 
	 * @param customer customer that wants to be added */
	public void msgIWantToEat(Customer customer)
	{
		synchronized(waitList) { waitList.add(new HostCustomer(customer)); }
		stateChanged();
	}
	
	public void msgImLeaving(Customer customer)
	{
		synchronized(waitList)
		{
			for (HostCustomer hc : waitList)
				if (hc.c.equals(customer))
					waitList.remove(customer);
		}
		stateChanged();
	}

	/** Waiter sends this message after the customer has left the table 
	 * @param tableNum table identification number */
	public void msgTableIsFree(int tableNum){
		tables[tableNum].occupied = false;
		stateChanged();
	}
	
	public void msgOutOfChoice(String choice)
	{
		this.menu.removeChoice(choice);
		menuUpdated = true;
		stateChanged();
	}
	
	public void msgItemInStock(String choice)
	{
		this.menu.restoreChoice(choice);
		menuUpdated = true;
		stateChanged();
	}
	
	public void msgCanGoOnBreak(Waiter waiter)
	{
		synchronized(waiters)
		{
			for (MyWaiter w : waiters)
			{
				if (w.wtr.equals(waiter))
					w.wantsBreak = true;
			}
		}
		stateChanged();
	}

	public void msgBackOnDuty(Waiter waiter)
	{
		synchronized(waiters)
		{
			for (MyWaiter w : waiters)
			{
				if (w.wtr.equals(waiter))
				{
					w.wantsBreak = false;
					w.working = true;
				}
			}
		}
		stateChanged();
	}

	//BOOKMARK
	/** Scheduler.  Determine what action is called for, and do it. */
	protected boolean pickAndExecuteAnAction() 
	{	
		if (menuUpdated)
		{
			this.notifyWaitersMenu();
			menuUpdated = false;
			return true;
		}

		int nextAvailableTable = -1;
		for(int i=0; i < nTables; i++)
		{
			if(!tables[i].occupied)
			{
				nextAvailableTable = i;
				break;
			}
		}

		if(!waitList.isEmpty() && !waiters.isEmpty())
		{
			if (nextAvailableTable != -1)
			{
				this.seatNextCustomer(nextAvailableTable);
				return true;
			} else {
				// No tables available, tell all waiting customers there is a wait
				this.tellCustomersThereIsWait();
			}
		}

		synchronized(waiters)
		{
			for (MyWaiter w : waiters)
			{
				if (w.wantsBreak && w.working)
				{
					boolean isOtherWaiterWorking = false;

					for (MyWaiter otherW : waiters)
						if (!otherW.equals(w))
							if (otherW.working)
							{
								isOtherWaiterWorking = true;
								break; // haha
							}

					if (isOtherWaiterWorking)
					{
						w.wtr.msgOkToGoOnBreak();
						System.out.println(w.wtr.getName() + " was allowed to go on break");
						w.working = false;
					} else {
						w.wtr.msgCannotGoOnBreak();
						System.out.println(w.wtr.getName() + " was not allowed to go on break");
						w.working = true;
						w.wantsBreak = false;
					}
					break;
				}
			}
		}

		//we have tried all our rules (in this case only one) and found
		//nothing to do. So return false to main loop of abstract agent
		//and wait.
		return false;
	}

	//BOOKMARK
	// *** ACTIONS ***

	/** Assigns a customer to a specified waiter and 
	 * tells that waiter which table to sit them at.
	 * @param waiter
	 * @param customer
	 * @param tableNum */
	private void tellWaiterToSitCustomerAtTable(MyWaiter waiter, Customer customer, int tableNum){
		print("Telling " + waiter.wtr + " to sit " + customer +" at table "+(tableNum+1));
		waiter.wtr.msgSitCustomerAtTable(customer, tableNum);
		tables[tableNum].occupied = true;
		synchronized(waitList) { waitList.remove(customer); }
		nextWaiter = (nextWaiter+1)%waiters.size();
		stateChanged();
	}
	
	private void notifyWaitersMenu()
	{
		synchronized(waiters)
		{
			for (MyWaiter w : waiters)
				w.wtr.msgMenuUpdated(menu);
		}
		
		menuUpdated = false;
	}
	
	private void tellCustomersThereIsWait()
	{
		synchronized(waitList)
		{
			for (HostCustomer hc : waitList)
				if (!hc.hasBeenAskedToWait)
				{
					hc.c.msgThereIsAWait();
					hc.hasBeenAskedToWait = true;
				}
		}
	}
	
	private void seatNextCustomer(int nextAvailableTable)
	{
		synchronized(waiters){
			//Finds the next waiter that is working
			while(!waiters.get(nextWaiter).working){
				nextWaiter = (nextWaiter+1)%waiters.size();
			}
		}
		print("picking waiter number: " + nextWaiter);
		//Then runs through the tables and finds the first unoccupied 
		//table and tells the waiter to sit the first customer at that table

		synchronized(waitList){
			tellWaiterToSitCustomerAtTable(waiters.get(nextWaiter),
					waitList.remove(0).c, nextAvailableTable);
		}
	}



	// *** EXTRA ***

	/** Returns the name of the host 
	 * @return name of host */
	public String getName(){
		return name;
	}    

	/** Hack to enable the host to know of all possible waiters 
	 * @param waiter new waiter to be added to list
	 */
	public void setWaiter(Waiter waiter){
		synchronized(waiters) { waiters.add(new MyWaiter(waiter)); }
		waiter.msgMenuUpdated(menu);
		stateChanged();
	}

	//Gautam Nayak - Gui calls this when table is created in animation
	public void addTable() {
		nTables++;
		Table[] tempTables = new Table[nTables];
		for(int i=0; i < nTables - 1; i++){
			tempTables[i] = tables[i];
		}  		  			
		tempTables[nTables - 1] = new Table(nTables - 1);
		tables = tempTables;
	}
}
