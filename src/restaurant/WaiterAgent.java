package restaurant;
import java.awt.Color;

import restaurant.interfaces.Cashier;
import restaurant.interfaces.Cook;
import restaurant.interfaces.Customer;
import restaurant.interfaces.Host;
import restaurant.interfaces.Waiter;
import restaurant.layoutGUI.*;
import agent.Agent;
import java.util.Timer;
import java.util.TimerTask;
import astar.*;
import java.util.*;
import java.util.concurrent.Semaphore;

/** Restaurant Waiter Agent.
 * Sits customers at assigned tables and takes their orders.
 * Takes the orders to the cook and then returns them 
 * when the food is done.  Cleans up the tables after the customers leave.
 * Interacts with customers, host, and cook */
public class WaiterAgent extends Agent implements Waiter
{

	//State variables for Waiter
	private boolean onBreak = false;
	private boolean wantBreak = false;
	private boolean haveBreakApproval = false;
	private boolean awaitingBreakApproval = false;

	//State constants for Customers
	public enum CustomerState {NEED_SEATED, READY_TO_ORDER, ORDER_PENDING, MUST_REORDER, RECHOOSING, ORDER_READY,
							   READY_FOR_BILL, BILL_BEING_MADE, WAITING_FOR_BILL, LEAVING, NO_ACTION};

	Timer timer = new Timer();

	/** Private class to hold information for each customer.
	 * Contains a reference to the customer, his choice, 
	 * table number, and state */
	private class WaiterCustomer {
		public CustomerState state;
		public Customer cAgent;
		public OrderSheet orderSheet = null; // Must instantiate when order is taken
		public int tableNum;
		public Food food; //gui thing
		public CustomerBill bill;

		/** Constructor for MyCustomer class.
		 * @param cmr reference to customer
		 * @param num assigned table number */
		public WaiterCustomer(Customer cAgent, int num){
			this.cAgent = cAgent;
			tableNum = num;
			state = CustomerState.NO_ACTION;
			bill = null;
		}
	}

	//Name of waiter
	private String name;

	//All the customers that this waiter is serving
	private List<WaiterCustomer> customers = Collections.synchronizedList(new ArrayList<WaiterCustomer>());

	private Host host;
	private Cook cook;
	private Cashier cashier;
	
	private Menu menu;
	
	private ProducerConsumerMonitor<OrderSheet> orderCarousel;
	private boolean usesCarousel;
	
	private Semaphore waitSem = new Semaphore(0);

	//Animation Variables
	AStarTraversal aStar;
	Restaurant restaurant; //the gui layout
	GuiWaiter guiWaiter; 
	Position currentPosition; 
	Position originalPosition;
	Table[] tables; //the gui tables


	/** Constructor for WaiterAgent class
	 * @param name name of waiter
	 * @param gui reference to the gui */
	public WaiterAgent(String name, AStarTraversal aStar, Restaurant restaurant, Table[] tables, 
					   ProducerConsumerMonitor<OrderSheet> orderCarousel, boolean usesCarousel) 
	{
		super();

		this.name = name;
		this.menu = new Menu();
		
		this.orderCarousel = orderCarousel;
		this.usesCarousel = usesCarousel;

		//initialize all the animation objects
		this.aStar = aStar;
		this.restaurant = restaurant;//the layout for astar
		guiWaiter = new GuiWaiter(name.substring(0,2), new Color(255, 0, 0), restaurant);
		currentPosition = new Position(guiWaiter.getX(), guiWaiter.getY());
		currentPosition.moveInto(aStar.getGrid());
		originalPosition = currentPosition;//save this for moving into
		this.tables = tables;
	}

	//BOOKMARK
	// *** MESSAGES ***

	/** Host sends this to give the waiter a new customer.
	 * @param customer customer who needs seated.
	 * @param tableNum identification number for table */
	public void msgSitCustomerAtTable(Customer customer, int tableNum){
		WaiterCustomer c = new WaiterCustomer(customer, tableNum);
		c.state = CustomerState.NEED_SEATED;
		synchronized(customers) { customers.add(c); }
		stateChanged();
	}

	/** Customer sends this when they are ready.
	 * @param customer customer who is ready to order.
	 */
	public void msgImReadyToOrder(Customer customer){
		synchronized(customers)
		{
			for(WaiterCustomer wc : customers)
			{
				if (wc.cAgent.equals(customer))
				{
					wc.state = CustomerState.READY_TO_ORDER;
					stateChanged();
					return;
				}
			}
		}

		System.out.println("msgImReadyToOrder in WaiterAgent, didn't find him?");
	}

	/** Customer sends this when they have decided what they want to eat 
	 * @param customer customer who has decided their choice
	 * @param choice the food item that the customer chose */
	public void msgHereIsMyChoice(Customer customer, String choice)
	{
		synchronized(customers)
		{
			for (WaiterCustomer c : customers)
			{
				if(c.cAgent.equals(customer))
				{
					c.orderSheet = new OrderSheet(this, c.tableNum, choice);
					c.state = CustomerState.ORDER_PENDING;
					
					waitSem.release();
					
					stateChanged();
					return;
				}
			}
		}
		
		System.err.println(this.name + " tried to take " + customer.getName() + "'s order, but couldn't find him!");
	}

	/** Cook sends this when the order is ready.
	 * @param tableNum identification number of table whose food is ready
	 * @param f is the guiFood object */
	public void msgOrderIsReady(int tableNum, Food f)
	{
		synchronized(customers)
		{
			for(WaiterCustomer c:customers)
			{
				if(c.tableNum == tableNum)
				{
					c.state = CustomerState.ORDER_READY;
					c.food = f; //so that later we can remove it from the table.
					stateChanged();
					return;
				}
			}
		}
	}

	/** Customer sends this when they are done eating.
	 * @param customer customer who is leaving the restaurant. */
	public void msgImLeaving(Customer customer)
	{
		synchronized(customers)
		{
			for(WaiterCustomer c:customers)
			{
				if(c.cAgent.equals(customer))
				{
					c.state = CustomerState.LEAVING;
					stateChanged();
					break;
				}
			}
		}
	}

	/**
	 * Sent from customer to waiter
	 * @param c
	 */
	public void msgReadyForBill(Customer c)
	{
		synchronized(customers)
		{
			for (WaiterCustomer wc : customers)
			{
				if (wc.cAgent.equals(c))
				{
					wc.state = CustomerState.READY_FOR_BILL;
					stateChanged();
					break;
				}
			}
		}
	}

	/**
	 * Sent from cashier to waiter.
	 * @param bill
	 */
	public void msgBillReady(CustomerBill bill)
	{	
		WaiterCustomer customer = null;
		synchronized(customers)
		{
			for (WaiterCustomer wc : customers)
			{
				if (wc.cAgent.getName().equals(bill.customer.getName()))
				{
					customer = wc;
					break;
				}
			}
		}
		
		if (customer != null)
		{
			customer.state = CustomerState.WAITING_FOR_BILL;
			customer.bill = bill;
			stateChanged();
		} else System.err.println("Bill could not be matched to a customer!");

	}
	
	public void msgMenuUpdated(Menu newMenu)
	{
		this.menu = newMenu;
		stateChanged();
	}
	
	/** Sent by cook */
	public void msgOutOfChoice(int table, String choice)
	{
		menu.removeChoice(choice);

		synchronized(customers)
		{
			for (WaiterCustomer wc : customers)
			{
				if (table == wc.tableNum)
				{
					wc.state = CustomerState.MUST_REORDER;
					break;
				}
			}
		}
		stateChanged();
	}
	
	/** Sent by host */
	public void msgOkToGoOnBreak()
	{
		this.haveBreakApproval = true;
		stateChanged();
	}
	
	/** Sent by host */
	public void msgCannotGoOnBreak()
	{
		this.haveBreakApproval = false;
		this.onBreak = false;
		this.wantBreak = false;
		stateChanged();
	}
	

	/** Sent from GUI to control breaks 
	 * @param state true when the waiter should go on break and 
	 *              false when the waiter should go off break
	 *              Is the name onBreak right? What should it be?*/
	public void setBreakStatus(boolean breaking){
		if (breaking)
		{
			wantBreak = true;
			this.awaitingBreakApproval = true;
		} else {
			if (onBreak)
				print("Coming off break");
			wantBreak = false;
			onBreak = false;
			haveBreakApproval = false;
			awaitingBreakApproval = false;
		}
		stateChanged();
	}

	//BOOKMARK
	/** Scheduler.  Determine what action is called for, and do it. */
	protected boolean pickAndExecuteAnAction() {
		//print("in waiter scheduler");

		//Runs through the customers for each rule, so 
		//the waiter doesn't serve only one customer at a time
		if (!customers.isEmpty())
		{
			WaiterCustomer c = null;

			c = this.findCustomerWithState(CustomerState.ORDER_READY);
			if (c != null) { giveFoodToCustomer(c); return true; }

			c = this.findCustomerWithState(CustomerState.MUST_REORDER);
			if (c != null) { getReorder(c); return true; }

			c = this.findCustomerWithState(CustomerState.LEAVING);
			if (c != null) { clearTable(c); return true; }

			c = this.findCustomerWithState(CustomerState.NEED_SEATED);
			if (c != null) { seatCustomer(c); return true; }

			c = this.findCustomerWithState(CustomerState.ORDER_PENDING);
			if (c != null) { giveOrderToCook(c); return true; }

			c = this.findCustomerWithState(CustomerState.READY_TO_ORDER);
			if (c != null) { takeOrder(c); return true; }

			c = this.findCustomerWithState(CustomerState.READY_FOR_BILL);
			if (c != null) { getBill(c); return true; }

			c = this.findCustomerWithState(CustomerState.WAITING_FOR_BILL);
			if (c != null) { giveBillToCustomer(c); return true; }
		}

		if (!currentPosition.equals(originalPosition)) {
			DoMoveToOriginalPosition();//Animation thing
			return true;
		}

		if (wantBreak && haveBreakApproval && customers.size() == 0)
		{
			this.onBreak = true;
			System.out.println(this.name + " is now on break.");
			return false;
		}
		
		// Have nothing left to do - ask to go on break
		if (wantBreak && awaitingBreakApproval && customers.size() == 0)
		{
			askToGoOnBreak();
			return true;
		}

		//we have tried all our rules and found nothing to do. 
		// So return false to main loop of abstract agent and wait.
		//print("in scheduler, no rules matched:");
		return false;
	}
	
	private WaiterCustomer findCustomerWithState(CustomerState desiredState)
	{		
		synchronized(customers)
		{
			for (WaiterCustomer wc : customers)
			{
				if (wc.state == desiredState)
				{
					return wc;
				}
			}
		}
		
		return null;
	}

	
	//BOOKMARK
	// *** ACTIONS ***

	/** Seats the customer at a specific table 
	 * @param customer customer that needs seated */
	private void seatCustomer(WaiterCustomer customer) {
		DoSeatCustomer(customer); //animation	
		customer.state = CustomerState.NO_ACTION;
		customer.cAgent.msgFollowMeToTable(this, new Menu());
		stateChanged();
	}
	/** Takes down the customers order 
	 * @param customer customer that is ready to order */
	private void takeOrder(WaiterCustomer customer) 
	{
		DoTakeOrder(customer); //animation
		customer.state = CustomerState.NO_ACTION;
		customer.cAgent.msgWhatWouldYouLike();
		
		// Make the waiter stand there until the customer says his order
		try {
			waitSem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		stateChanged();
	}

	/** Gives any pending orders to the cook 
	 * @param customer customer that needs food cooked */
	private void giveOrderToCook(WaiterCustomer customer) {
		//In our animation the waiter does not move to the cook in
		//order to give him an order. We assume some sort of electronic
		//method implemented as our message to the cook. So there is no
		//animation analog, and hence no DoXXX routine is needed.

		customer.state = CustomerState.NO_ACTION;
		
		if (usesCarousel)
		{
			print("Putting " + customer.cAgent + "'s choice of " + customer.orderSheet.choice + " in the order carousel");
			orderCarousel.insert(customer.orderSheet);
			cook.msgNewOrder();
		} else {
			print("Giving " + customer.cAgent + "'s choice of " + customer.orderSheet.choice + " to cook");
			cook.msgHereIsAnOrder(customer.orderSheet);
		}
		stateChanged();

		//Here's a little animation hack. We put the first two
		//character of the food name affixed with a ? on the table.
		//Simply let's us see what was ordered.
		tables[customer.tableNum].takeOrder(customer.orderSheet.choice.substring(0,2)+"?");
		restaurant.placeFood(tables[customer.tableNum].foodX(),
				tables[customer.tableNum].foodY(),
				new Color(255, 255, 255), customer.orderSheet.choice.substring(0,2)+"?");
	}

	/** Gives food to the customer 
	 * @param customer customer whose food is ready */
	private void giveFoodToCustomer(WaiterCustomer customer) {
		DoGiveFoodToCustomer(customer);//Animation
		customer.state = CustomerState.NO_ACTION;
		customer.cAgent.msgHereIsYourFood(customer.orderSheet.choice);
		stateChanged();
	}
	/** Starts a timer to clear the table 
	 * @param customer customer whose table needs cleared */
	private void clearTable(WaiterCustomer customer) {
		DoClearingTable(customer);
		customer.state = CustomerState.NO_ACTION;
		stateChanged();
	}
	
	private void getReorder(WaiterCustomer c)
	{
		c.cAgent.msgPleaseReorder(menu);
		// Change the customer's state so that the waiter doesn't keep asking the customer to reorder
		c.state = CustomerState.RECHOOSING; 
		stateChanged();
	}
	
	private void getBill(WaiterCustomer c)
	{
		cashier.msgNeedBill(this, c.cAgent, c.tableNum, c.orderSheet.choice);
		c.state = CustomerState.BILL_BEING_MADE;
		stateChanged();
	}
	
	private void giveBillToCustomer(WaiterCustomer c)
	{
		c.cAgent.msgHereIsBill(c.bill);
		c.state = CustomerState.NO_ACTION;
		print("Waiter " + name + " gave $" + c.bill.checkCharge() + " check to customer " + c.cAgent.getName());
		stateChanged();
	}
	
	private void askToGoOnBreak()
	{
		System.out.println(this.name + " wants to go on break");
		host.msgCanGoOnBreak(this);
		this.awaitingBreakApproval = false;
		stateChanged();
	}
	

	// Animation Actions
	void DoSeatCustomer (WaiterCustomer customer){
		print("Seating " + customer.cAgent + " at table " + (customer.tableNum+1));
		//move to customer first.
		GuiCustomer guiCustomer = customer.cAgent.getGuiCustomer();
		guiMoveFromCurrentPostionTo(new Position(guiCustomer.getX()+1,guiCustomer.getY()));
		guiWaiter.pickUpCustomer(guiCustomer);
		Position tablePos = new Position(tables[customer.tableNum].getX()-1,
				tables[customer.tableNum].getY()+1);
		guiMoveFromCurrentPostionTo(tablePos);
		guiWaiter.seatCustomer(tables[customer.tableNum]);
	}
	void DoTakeOrder(WaiterCustomer customer){
		print("Taking " + customer.cAgent +"'s order.");
		Position tablePos = new Position(tables[customer.tableNum].getX()-1,
				tables[customer.tableNum].getY()+1);
		guiMoveFromCurrentPostionTo(tablePos);
	}
	void DoGiveFoodToCustomer(WaiterCustomer customer){
		print("Giving finished order of " + customer.orderSheet.choice +" to " + customer.cAgent);
		Position inFrontOfGrill = new Position(customer.food.getX()-1,customer.food.getY());
		guiMoveFromCurrentPostionTo(inFrontOfGrill);//in front of grill
		guiWaiter.pickUpFood(customer.food);
		Position tablePos = new Position(tables[customer.tableNum].getX()-1,
				tables[customer.tableNum].getY()+1);
		guiMoveFromCurrentPostionTo(tablePos);
		guiWaiter.serveFood(tables[customer.tableNum]);
	}
	void DoClearingTable(final WaiterCustomer customer){
		print("Clearing table " + (customer.tableNum+1) + " (1500 milliseconds)");
		timer.schedule(new TimerTask(){
			public void run(){		    
				endCustomer(customer);
			}
		}, 1500);
	}
	/** Function called at the end of the clear table timer
	 * to officially remove the customer from the waiter's list.
	 * @param customer customer who needs removed from list */
	private void endCustomer(WaiterCustomer customer)
	{ 
		print("Table " + (customer.tableNum+1) + " is cleared!");
		customer.food.remove(); //remove the food from table animation
		host.msgTableIsFree(customer.tableNum);
		synchronized(customers) { customers.remove(customer); }
		stateChanged();
	}
	private void DoMoveToOriginalPosition(){
		print("Nothing to do. Moving to original position="+originalPosition);
		guiMoveFromCurrentPostionTo(originalPosition);
	}

	//this is just a subroutine for waiter moves. It's not an "Action"
	//itself, it is called by Actions.
	void guiMoveFromCurrentPostionTo(Position to){
		//System.out.println("[Gaut] " + guiWaiter.getName() + " moving from " + currentPosition.toString() + " to " + to.toString());

		AStarNode aStarNode = (AStarNode)aStar.generalSearch(currentPosition, to);
		List<Position> path = aStarNode.getPath();
		Boolean firstStep   = true;
		Boolean gotPermit   = true;

		for (Position tmpPath: path) {
			//The first node in the path is the current node. So skip it.
			if (firstStep) {
				firstStep   = false;
				continue;
			}

			//Try and get lock for the next step.
			int attempts    = 1;
			gotPermit       = new Position(tmpPath.getX(), tmpPath.getY()).moveInto(aStar.getGrid());

			//Did not get lock. Lets make n attempts.
			while (!gotPermit && attempts < 3) {
				//System.out.println("[Gaut] " + guiWaiter.getName() + " got NO permit for " + tmpPath.toString() + " on attempt " + attempts);

				//Wait for 1sec and try again to get lock.
				try { Thread.sleep(1000); }
				catch (Exception e){}

				gotPermit   = new Position(tmpPath.getX(), tmpPath.getY()).moveInto(aStar.getGrid());
				attempts ++;
			}

			//Did not get lock after trying n attempts. So recalculating path.            
			if (!gotPermit) {
				//System.out.println("[Gaut] " + guiWaiter.getName() + " No Luck even after " + attempts + " attempts! Lets recalculate");
				guiMoveFromCurrentPostionTo(to);
				break;
			}

			//Got the required lock. Lets move.
			//System.out.println("[Gaut] " + guiWaiter.getName() + " got permit for " + tmpPath.toString());
			currentPosition.release(aStar.getGrid());
			currentPosition = new Position(tmpPath.getX(), tmpPath.getY ());
			guiWaiter.move(currentPosition.getX(), currentPosition.getY());
		}
		/*
	boolean pathTaken = false;
	while (!pathTaken) {
	    pathTaken = true;
	    //print("A* search from " + currentPosition + "to "+to);
	    AStarNode a = (AStarNode)aStar.generalSearch(currentPosition,to);
	    if (a == null) {//generally won't happen. A* will run out of space first.
		System.out.println("no path found. What should we do?");
		break; //dw for now
	    }
	    //dw coming. Get the table position for table 4 from the gui
	    //now we have a path. We should try to move there
	    List<Position> ps = a.getPath();
	    Do("Moving to position " + to + " via " + ps);
	    for (int i=1; i<ps.size();i++){//i=0 is where we are
		//we will try to move to each position from where we are.
		//this should work unless someone has moved into our way
		//during our calculation. This could easily happen. If it
		//does we need to recompute another A* on the fly.
		Position next = ps.get(i);
		if (next.moveInto(aStar.getGrid())){
		    //tell the layout gui
		    guiWaiter.move(next.getX(),next.getY());
		    currentPosition.release(aStar.getGrid());
		    currentPosition = next;
		}
		else {
		    System.out.println("going to break out path-moving");
		    pathTaken = false;
		    break;
		}
	    }
	}
		 */
	}

	// *** EXTRA ***

	/** @return name of waiter */
	public String getName(){
		return name;
	}

	/** @return string representation of waiter */
	public String toString(){
		return "waiter " + getName();
	}

	/** Hack to set the cook for the waiter */
	public void setCook(Cook cook){
		this.cook = cook;
	}

	/** Hack to set the host for the waiter */
	public void setHost(Host host){
		this.host = host;
	}
	
	public void setCashier(Cashier cashier)
	{
		this.cashier = cashier;
	}

	/** @return true if the waiter is on break, false otherwise */
	public boolean isOnBreak(){
		return onBreak;
	}

}

