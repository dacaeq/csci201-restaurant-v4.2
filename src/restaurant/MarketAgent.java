package restaurant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import restaurant.MarketOrder.MarketOrderState;
import restaurant.interfaces.Market;

import agent.Agent;

/**
 * 
 * @author Clayton Ketner
 *
 */
public class MarketAgent extends Agent implements Market
{
	private String name;
	
	private List<MarketOrder> orders = Collections.synchronizedList(new ArrayList<MarketOrder>());
	private ArrayList<MarketItem> inventory;
	private HashMap<String, Integer> prices = new HashMap<String, Integer>();
	private int wallet;
	
	Timer timer = new Timer();
	
	public MarketAgent(String name)
	{
		super();
		this.name = name;
		
		inventory = new ArrayList<MarketItem>();
		inventory.add(new MarketItem("Steak", 10, 3));
		inventory.add(new MarketItem("Chicken", 10, 2));
		inventory.add(new MarketItem("Pizza", 10, 1));
		inventory.add(new MarketItem("Salad", 10, 1));

		prices.put("Steak", 3);
		prices.put("Chicken", 2);
		prices.put("Pizza", 1);
		prices.put("Salad", 1);
		
		wallet = 0;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getMoney()
	{
		return wallet;
	}
	
	/** Used by gui */
	public ArrayList<MarketItem> getInventory()
	{
		return inventory;
	}
	
	public void changeQuantityOfItem(String itemName, int delta)
	{
		for (MarketItem item : inventory)
		{
			if (item.getName().equals(itemName))
			{
				item.addUnits(delta);
			}
		}
	}
	
	public void setPrices(HashMap<String, Integer> prices)
	{
		this.prices = prices;
	}
	
	// ** MESSAGING **
	public void msgOrderSupplies(MarketOrder order)
	{
		synchronized(orders) { orders.add(order); }
		
		stateChanged();
	}
	
	public void msgHereIsPayment(MarketBill bill)
	{
		wallet += bill.getPayment();
		stateChanged();
	}

	protected boolean pickAndExecuteAnAction() 
	{
		if (orders.size() == 0)
			return false;
		
		MarketOrder m = null;
		
		m = findMOWithState(MarketOrderState.READY_TO_SHIP);
		if (m != null) { sendShipment(m); return true; }
		
		m = findMOWithState(MarketOrderState.PENDING);
		if (m != null) { prepareShipment(m); m.state = MarketOrderState.READY_TO_SHIP; return true; }
		
		m = findMOWithState(MarketOrderState.SHIPPED);
		if (m != null) { notifyShipmentComplete(m); return true; }
		
		return false;
	}
	
	private MarketOrder findMOWithState(MarketOrderState findState)
	{
		synchronized(orders)
		{
		for (MarketOrder m : orders)
			if (m.state == findState)
				return m;
		}
		
		return null;
	}

	// ** ACTIONS **
	private void prepareShipment(MarketOrder m)
	{
		TreeMap<String, Integer> orderItems = m.getOrderItems();
		for (String choice : orderItems.keySet())
		{
			for (MarketItem inventoryItem : inventory)
			{
				if (inventoryItem.getName().equals(choice))
				{
					// Remove items from inventory and add to MarketOrder shipment
					m.addItemToShipment(inventoryItem.removeUnits(orderItems.get(choice)));
					break;
				}
			}
		}
		
		print("Prepared the following shipment:");
		
		for (MarketItem mi : m.getShipmentManifest())
			print("\t" + mi.getQuantity() + " - " + mi.getName());
		
		m.state = MarketOrderState.READY_TO_SHIP;
	}
	
	private void sendShipment(final MarketOrder m)
	{
		m.cook.msgWillSend(m, (int)m.getShippingTime());
		
		// Check if the shipment actually contained anything - if not, throw it out
		boolean shipmentContainsItems = false;
		
		for (MarketItem mi : m.getShipmentManifest())
			if (mi.getQuantity() > 0)
				shipmentContainsItems = true;
		
		if (!shipmentContainsItems)
		{
			orders.remove(m);
			return;
		}
			
		
		m.state = MarketOrderState.SHIPPING;
		System.out.println("A shipment is on its way from " + this.name + " and will arrive in " + (int)(m.getShippingTime()) + " seconds.");
		timer = new Timer();
		timer.schedule(new TimerTask(){
			public void run(){//this routine is like a message reception    
				m.state = MarketOrderState.SHIPPED;
				stateChanged();
			}
		}, (int)(m.getShippingTime()*1000));
		
		stateChanged();
	}
	
	private void sendBill(MarketOrder m)
	{
		m.calculateBill(prices);
		m.cashier.msgHereIsBill(m.bill);
		stateChanged();
	}
	
	private void notifyShipmentComplete(MarketOrder m)
	{
		sendBill(m);
		m.cook.msgSuppliesShipped(m.getShipmentManifest(), m);
		
		synchronized(orders)
		{
			orders.remove(m);
		}
		
		stateChanged();
	}
	
	
}


































