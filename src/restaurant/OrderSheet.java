package restaurant;

import restaurant.interfaces.Waiter;

/**
 * 
 * @author Clayton Ketner
 *
 */
public class OrderSheet
{
	final Waiter waiter;
	final int table;
	final String choice;
	
	public OrderSheet(Waiter waiter, int table, String choice)
	{
		this.waiter = waiter;
		this.table = table;
		this.choice = choice;
	}
	
	public Waiter getWaiter()
	{
		return waiter;
	}
	
	public int getTable()
	{
		return table;
	}
	
	public String getChoice()
	{
		return choice;
	}
}
