package restaurant.gui;

import restaurant.MarketAgent;
import restaurant.interfaces.Customer;
import restaurant.interfaces.Market;
import restaurant.interfaces.Waiter;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;


/** Main GUI class.
 * Contains the main frame and subsequent panels */
@SuppressWarnings("serial")
public class RestaurantGui extends JFrame implements ActionListener{

	private final int WINDOWX = 800;
	private final int WINDOWY = 500;

	private RestaurantPanel restPanel = new RestaurantPanel(this);
	private JPanel infoPanel = new JPanel();
	private JLabel infoLabel = new JLabel(
			"<html><pre><i>(Click on a customer/waiter)</i></pre></html>");
	private JCheckBox stateCB = new JCheckBox();
	private JLabel currentMoney = new JLabel("$--");
	private JButton addDollar = new JButton("$++");
	private JButton subDollar = new JButton("$--");
	private JButton addTable = new JButton("Add Table");
	
	private ArrayList<MarketPanel> marketPanels = new ArrayList<MarketPanel>();
	private JButton addMarket = new JButton("Add market");
	
	private JPanel marketListPanel = new JPanel();
	
	Timer refreshTimer;

	private Object currentPerson;

	/** Constructor for RestaurantGui class.
	 * Sets up all the gui components. */
	public RestaurantGui(){

		super("Restaurant Application");
		
		refreshTimer = new Timer(100, this);
		refreshTimer.setActionCommand("refreshTimer");
		refreshTimer.start();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(50,50, WINDOWX, WINDOWY);

		getContentPane().setLayout(new BoxLayout((Container)getContentPane(),BoxLayout.Y_AXIS));

		Dimension rest = new Dimension(WINDOWX, (int)(WINDOWY*.6));
		Dimension info = new Dimension(WINDOWX, (int)(WINDOWY*.25));
		restPanel.setPreferredSize(rest);
		restPanel.setMinimumSize(rest);
		infoPanel.setPreferredSize(info);
		infoPanel.setMinimumSize(info);
		infoPanel.setBorder(BorderFactory.createTitledBorder("Information"));

		stateCB.setVisible(false);
		stateCB.addActionListener(this);
		currentMoney.setVisible(false);
		addDollar.setVisible(false);
		addDollar.addActionListener(this);
		addDollar.setActionCommand("+");
		subDollar.setVisible(false);
		subDollar.addActionListener(this);
		subDollar.setActionCommand("-");

		infoPanel.setLayout(new GridLayout(1,2, 30,0));
		infoPanel.add(infoLabel);
		infoPanel.add(stateCB);
		infoPanel.add(currentMoney);
		infoPanel.add(addDollar);
		infoPanel.add(subDollar);

		getContentPane().add(restPanel);
		getContentPane().add(addTable);
		getContentPane().add(infoPanel);

		addTable.addActionListener(this);
		
		addMarket.setActionCommand("addMarket");
		addMarket.addActionListener(this);
		getContentPane().add(addMarket);
		
		marketListPanel.setLayout(new BoxLayout(marketListPanel, BoxLayout.X_AXIS));
		JScrollPane scrollPane = new JScrollPane(marketListPanel);
		getContentPane().add(scrollPane);
	}


	/** This function takes the given customer or waiter object and 
	 * changes the information panel to hold that person's info.
	 * @param person customer or waiter object */
	public void updateInfoPanel(Object person){
		stateCB.setVisible(true);
		currentPerson = person;

		if(person instanceof Customer){
			currentMoney.setVisible(true);
			addDollar.setVisible(true);
			subDollar.setVisible(true);

			Customer customer = (Customer) person;
			stateCB.setText("Hungry?");
			stateCB.setSelected(customer.isHungry());
			stateCB.setEnabled(!customer.isHungry());
			infoLabel.setText(
					"<html><pre>     Name: " + customer.getName() + " </pre></html>");

		}else if(person instanceof Waiter){
			currentMoney.setVisible(false);
			addDollar.setVisible(false);
			subDollar.setVisible(false);

			Waiter waiter = (Waiter) person;
			stateCB.setText("On Break?");
			stateCB.setSelected(waiter.isOnBreak());
			stateCB.setEnabled(true);
			infoLabel.setText(
					"<html><pre>     Name: " + waiter.getName() + " </html>");
		}	   

		infoPanel.validate();
	}

	/** Action listener method that reacts to the checkbox being clicked */
	public void actionPerformed(ActionEvent e){

		if(e.getSource() == stateCB){
			if(currentPerson instanceof Customer){
				Customer c = (Customer) currentPerson;
				c.setHungry();
				stateCB.setEnabled(false);

			}else if(currentPerson instanceof Waiter){
				Waiter w = (Waiter) currentPerson;
				w.setBreakStatus(stateCB.isSelected());
			}
		}
		else if (e.getSource() == addTable)
		{
			try {
				System.out.println("[Gautam] Add Table!");
				//String XPos = JOptionPane.showInputDialog("Please enter X Position: ");
				//String YPos = JOptionPane.showInputDialog("Please enter Y Position: ");
				//String size = JOptionPane.showInputDialog("Please enter Size: ");
				//restPanel.addTable(10, 5, 1);
				//restPanel.addTable(Integer.valueOf(YPos).intValue(), Integer.valueOf(XPos).intValue(), Integer.valueOf(size).intValue());
				restPanel.addTable();
			}
			catch(Exception ex) {
				System.out.println("Unexpected exception caught in during setup:"+ ex);
			}
		}
		
		if (e.getActionCommand().equals("refreshTimer"))
		{
			if (currentPerson instanceof Customer)
			{
				Customer cust = (Customer)currentPerson;
				currentMoney.setText("$" + cust.getCash());
			}
			
			this.validate();
		}
		
		if (e.getActionCommand().equals("+"))
		{
			if (currentPerson instanceof Customer)
			{
				Customer cust = (Customer)currentPerson;
				cust.setCash(cust.getCash() + 1);
			}
		}
		
		if (e.getActionCommand().equals("-"))
		{
			if (currentPerson instanceof Customer)
			{
				Customer cust = (Customer)currentPerson;
				cust.setCash(cust.getCash() - 1);
			}
		}
		
		if (e.getActionCommand().equals("addMarket"))
		{
			Market newMarket = new MarketAgent("m" + (marketPanels.size() + 1));
			System.err.println("Adding market " + newMarket.getName());
			
			MarketPanel marketPanel = new MarketPanel(newMarket);
			marketPanels.add(marketPanel);
			marketListPanel.add(marketPanel);
			
			newMarket.startThread();
			restPanel.cook.addMarket(newMarket);
		}

	}

	/** Message sent from a customer agent to enable that customer's 
	 * "I'm hungery" checkbox.
	 * @param c reference to the customer */
	public void setCustomerEnabled(Customer c){
		if(currentPerson instanceof Customer){
			Customer cust = (Customer) currentPerson;
			if(c.equals(cust)){
				stateCB.setEnabled(true);
				stateCB.setSelected(false);
			}
		}
	}


	/** Main routine to get gui started */
	public static void main(String[] args){
		RestaurantGui gui = new RestaurantGui();
		gui.setVisible(true);
		gui.setResizable(true);
	}
}
