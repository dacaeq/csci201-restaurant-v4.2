package restaurant;

import java.util.HashMap;

/**
 * 
 * @author Clayton Ketner
 *
 */
public interface BillModel 
{
	/**
	 * Call to add an item to the bill. Only works if the bill hasn't been calculated yet.
	 * @param choice
	 * @param quantity
	 * @return Returns false if the bill has already been calculated.
	 */
	public boolean addItem(String choice, int quantity);
	
	/**
	 * Call to calculate the charge of the bill.
	 * @param prices - TreeMap linking choice to price
	 */
	public void calculate(final HashMap<String, Integer> prices);
}
