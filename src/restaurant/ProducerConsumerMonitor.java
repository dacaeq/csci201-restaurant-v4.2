package restaurant;

import java.util.Vector;

public class ProducerConsumerMonitor<E>
{
	private final int N = 5;
    private int count = 0;
    private Vector<E> theData;
    
    public ProducerConsumerMonitor()
    {
        theData = new Vector<E>();
    }
    
    synchronized public void insert(E data) {
        while (count == N) {
            try{ 
                System.out.println("\tFull, waiting");
                wait(5000);                         // Full, wait to add
            } catch (InterruptedException ex) {};
        }
            
        insertItem(data);
        count++;
        if(count == 1) {
            System.out.println("\tNot Empty, notify");
            notify();                               // Not empty, notify a 
                                                    // waiting consumer
        }
    }
    
    synchronized public E remove() {
        E data;
        while(count == 0)
        {
            try{ 
                System.out.println("\tEmpty, waiting");
                wait(5000);                         // Empty, wait to consume
            } catch (InterruptedException ex) {};
        }

        data = removeItem();
        count--;
        if(count == N-1){ 
            System.out.println("\tNot full, notify");
            notify();                               // Not full, notify a waiting producer
        }
        return data;
    }
    
    private void insertItem(E data){
        theData.addElement(data);
    }
    
    private E removeItem(){
        E data = (E) theData.firstElement();
        theData.removeElementAt(0);
        return data;
    }
}

































