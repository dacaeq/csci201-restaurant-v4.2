package restaurant;

import restaurant.interfaces.Customer;
import restaurant.interfaces.Waiter;

/**
 * 
 * @author Clayton Ketner
 *
 */
public class CustomerBill extends Bill
{
	public Waiter waiter;
	public Customer customer;
	public int table;
	
	public CustomerBill(Waiter waiter, Customer customer, int table)
	{
		super();
		this.waiter = waiter;
		this.customer = customer;
		this.table = table;
	}
	
}
