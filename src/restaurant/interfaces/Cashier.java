package restaurant.interfaces;

import restaurant.Bill;

public interface Cashier {

	void msgNeedBill(Waiter waiter, Customer customer, int tableNum, String choice);

	void msgHereIsBill(Bill bill);

	void startThread();

}
