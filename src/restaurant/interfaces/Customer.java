package restaurant.interfaces;

import restaurant.CustomerBill;
import restaurant.Menu;
import restaurant.Receipt;
import restaurant.layoutGUI.GuiCustomer;

public interface Customer 
{

	String getName();
	
	void msgThereIsAWait();
	
	void msgFollowMeToTable(Waiter waiterAgent, Menu menu);
	
	void msgThankYouForComing(Receipt receipt);

	void msgWashDishes(int duration);

	void msgWhatWouldYouLike();

	void msgHereIsYourFood(String choice);

	void msgPleaseReorder(Menu menu);

	void msgHereIsBill(CustomerBill bill);

	GuiCustomer getGuiCustomer();

	boolean isHungry();

	int getCash();

	void setCash(int amt);

	void setHungry();

	void setHost(Host host);

	void setCashier(Cashier cashier);

	void startThread();


}
