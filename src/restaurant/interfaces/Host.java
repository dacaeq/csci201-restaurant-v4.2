package restaurant.interfaces;


public interface Host {

	void msgCanGoOnBreak(Waiter waiter);

	void msgTableIsFree(int tableNum);

	void msgOutOfChoice(String key);

	void msgItemInStock(String key);

	void msgIWantToEat(Customer customer);

	void msgImLeaving(Customer customer);

	void startThread();

	String getName();

	void setWaiter(Waiter w);

	void addTable();

}
