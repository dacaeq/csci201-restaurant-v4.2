package restaurant.interfaces;

import java.util.ArrayList;

import restaurant.MarketBill;
import restaurant.MarketItem;
import restaurant.MarketOrder;

public interface Market 
{
	
	void msgHereIsPayment(MarketBill mbill);

	void msgOrderSupplies(MarketOrder mo);

	String getName();

	ArrayList<MarketItem> getInventory();

	void changeQuantityOfItem(String item, int delta);
	
	void startThread();

}
