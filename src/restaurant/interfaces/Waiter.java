package restaurant.interfaces;

import restaurant.CustomerBill;
import restaurant.Menu;
import restaurant.layoutGUI.Food;

public interface Waiter {

	void msgBillReady(CustomerBill bill);

	void msgOrderIsReady(int tableNum, Food food);

	void msgOutOfChoice(int tableNum, String choice);

	void msgImReadyToOrder(Customer customer);

	void msgHereIsMyChoice(Customer customer, String choice);

	void msgReadyForBill(Customer customer);

	void msgImLeaving(Customer customer);

	void msgOkToGoOnBreak();

	void msgCannotGoOnBreak();

	String getName();

	void msgSitCustomerAtTable(Customer customer, int tableNum);

	void msgMenuUpdated(Menu menu);

	boolean isOnBreak();

	void setBreakStatus(boolean selected);

}
