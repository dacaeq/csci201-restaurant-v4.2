package restaurant.interfaces;

import java.util.ArrayList;
import java.util.HashMap;

import restaurant.MarketItem;
import restaurant.MarketOrder;
import restaurant.OrderSheet;

public interface Cook {

	void msgHereIsAnOrder(OrderSheet orderSheet);
	
	void msgNewOrder();

	void msgWillSend(MarketOrder m, int shippingTime);

	void msgSuppliesShipped(ArrayList<MarketItem> shipmentManifest,
			MarketOrder m);

	HashMap<String, Integer> getInventory();

	void changeQuantityOfItem(String item, int delta);

	void addMarket(Market newMarket);

	void startThread();

	void setCashier(Cashier cashier);

	void setHost(Host host);

	String getName();

}
