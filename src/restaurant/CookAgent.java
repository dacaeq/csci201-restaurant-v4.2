package restaurant;

import agent.Agent;

import java.util.*;

import restaurant.interfaces.Cashier;
import restaurant.interfaces.Cook;
import restaurant.interfaces.Host;
import restaurant.interfaces.Market;
import restaurant.interfaces.Waiter;
import restaurant.layoutGUI.*;
import java.awt.Color;

/** Cook agent for restaurant.
 *  Keeps a list of orders for waiters
 *  and simulates cooking them.
 *  Interacts with waiters only.
 */
public class CookAgent extends Agent implements Cook
{
	//List of all the orders
	private List<Order> orders = Collections.synchronizedList(new ArrayList<Order>());
	private Map<String,FoodData> inventory = Collections.synchronizedMap(new HashMap<String,FoodData>());
	public enum Status {PENDING, COOKING, DONE}; // order status

	private List<Market> markets = Collections.synchronizedList(new ArrayList<Market>());
	private int currentMarketIndex = 0; // Zero indexed
	private int marketTryCount = 0; // Used to keep track of whether all markets have been tried
	private boolean tryingToFillInventory = false;
	private boolean tryNextMarket = false;
	private List<MarketOrder> marketOrders = Collections.synchronizedList(new ArrayList<MarketOrder>());
	private List<ArrayList<MarketItem>> shipments = Collections.synchronizedList(new ArrayList<ArrayList<MarketItem>>());
	private Map<String, Integer> totals = Collections.synchronizedMap(new HashMap<String, Integer>()); // Holds the quantities, taking into account placed market orders

	//Name of the cook
	private String name;

	private Cashier cashier;
	private Host host;
	private ProducerConsumerMonitor<OrderSheet> orderCarousel;

	//Timer for simulation
	Timer timer = new Timer();
	Restaurant restaurant; //Gui layout

	/** Constructor for CookAgent class
	 * @param name name of the cook
	 */
	public CookAgent(String name, Restaurant restaurant, ProducerConsumerMonitor<OrderSheet> orderCarousel) {
		super();

		this.name = name;
		this.restaurant = restaurant;
		this.orderCarousel = orderCarousel;
		
		//Create the restaurant's inventory.
		inventory.put("Steak",new FoodData("Steak", 5, 2, 5, 20));
		inventory.put("Chicken",new FoodData("Chicken", 4, 15, 5, 20));
		inventory.put("Pizza",new FoodData("Pizza", 3, 0, 5, 20));
		inventory.put("Salad",new FoodData("Salad", 2, 0, 5, 20));
		
		// Fill the totals hashmap with the quantities in the inventory hashmap
		for (String key : inventory.keySet())
			totals.put(key, inventory.get(key).quantity);
	}

	public void addMarket(Market market)
	{
		synchronized (markets) { markets.add(market); }
	}

	/** Private class to store information about food.
	 *  Contains the food type, its cooking time, and ...
	 */
	private class FoodData {
		String type; //kind of food
		double cookTime;
		int quantity;
		int low;
		int high;
		// other things ...

		public FoodData(String type, double cookTime, int quantity, int low, int high){
			this.type = type;
			this.cookTime = cookTime;
			this.quantity = quantity;
			this.low = low;
			this.high = high;
		}

		public boolean isLow()
		{
			return (this.quantity <= this.low);
		}
	}
	/** Private class to store order information.
	 *  Contains the waiter, table number, food item,
	 *  cooktime and status.
	 */
	private class Order {
		public final Waiter waiter;
		public final int table;
		public final String choice;
		public Status status;
		public Food food; //a gui variable

		/** Constructor for Order class 
		 * @param waiter waiter that this order belongs to
		 * @param tableNum identification number for the table
		 * @param choice type of food to be cooked 
		 */
		public Order(OrderSheet os)
		{
			this.waiter = os.waiter;
			this.table = os.table;
			this.choice = os.choice;
			this.status = Status.PENDING;
		}

		/** Represents the object as a string */
		public String toString(){
			return choice + " for " + waiter;
		}
	}

	/** Used by the GUI to see the cook's inventory */
	public HashMap<String, Integer> getInventory()
	{
		HashMap<String, Integer> foodCount = new HashMap<String, Integer>();

		synchronized(inventory)
		{
			for (String key : inventory.keySet())
			{
				foodCount.put(key, inventory.get(key).quantity);
			}
		}

		return foodCount;
	}

	/** Used by the GUI to set the cook's inventory */
	public void changeQuantityOfItem(String choice, int delta)
	{
		for (FoodData fd : inventory.values())
			if (fd.type.equals(choice))
				fd.quantity += delta;
		
		synchronized(totals) { totals.put(choice, totals.get(choice) + delta); }
	}


	//BOOKMARK
	// *** MESSAGES ***
	
	public void msgHereIsAnOrder(OrderSheet order)
	{
		synchronized(orders) { orders.add(new Order(order)); }
		stateChanged();
	}
	
	public void msgNewOrder()
	{
		synchronized(orders) { orders.add(new Order(orderCarousel.remove())); }
		stateChanged();
	}
	
	/**
	 * Sent by market to cook.
	 * @param order - what will be shipped
	 * @param shippingDelay - time in seconds for the shipment to arrive
	 */
	public void msgWillSend(MarketOrder order, int shippingDelay)
	{		
		synchronized(marketOrders) { marketOrders.add(order); }
		tryNextMarket = true;
		stateChanged();
	}

	public void msgSuppliesShipped(ArrayList<MarketItem> shipment, MarketOrder mo)
	{
		System.out.println("A shipment from " + mo.market.getName() + " has arrived at the restaurant!");

		synchronized(shipments) { shipments.add(shipment); }
		stateChanged();
	}

	//BOOKMARK
	/** Scheduler.  Determine what action is called for, and do it. */
	protected boolean pickAndExecuteAnAction()
	{
		if (marketOrders.size() > 0)
		{
			MarketOrder nextMO = null;
			synchronized (marketOrders) { nextMO = marketOrders.remove(0); }
			checkMarketOrder(nextMO);
			return true;
		}
		
		if (tryingToFillInventory && tryNextMarket)
		{
			this.checkInventory(false);
			return true;
		}
		
		// Unpack any shipments
		if (shipments.size() > 0)
		{
			synchronized(shipments) { this.unpackShipment(shipments.remove(0)); }
			return true;
		}

		Order o = null;
		//If there exists an order o whose status is done, place o.
		o = this.findOrderWithState(Status.DONE);
		if (o != null) { placeOrder(o); return true; }

		//If there exists an order o whose status is pending, check the inventory, ordering only if quantities are low
		o = this.findOrderWithState(Status.PENDING);
		if (o != null){ checkInventory(true); cookOrder(o); return true; }	
		
		return false;
	}

	private Order findOrderWithState(Status orderStatus)
	{
		synchronized(orders)
		{
			for (Order o : orders)
				if (o.status == orderStatus)
					return o;
		}

		return null;
	}


	//BOOKMARK
	// *** ACTIONS ***

	/** Starts a timer for the order that needs to be cooked. 
	 * @param order
	 */
	private void cookOrder(Order order){
		// Check if the order was removed by checkInventory()
		if (!orders.contains(order))
			return;
		
		DoCooking(order);
		synchronized(inventory) { inventory.get(order.choice).quantity--; }
		synchronized(totals) { totals.put(order.choice, totals.get(order.choice) - 1); }
		order.status = Status.COOKING;
	}

	private void placeOrder(Order order)
	{
		DoPlacement(order);
		order.waiter.msgOrderIsReady(order.table, order.food);
		synchronized(orders) { orders.remove(order); }
	}

	private void checkInventory(boolean checkIfLow)
	{
		// Check if quantities are <= low and reorder if they are
		MarketOrder mo = null;
		synchronized (markets)
		{
			if (markets.size() > 0)
				mo = new MarketOrder(this, this.cashier, markets.get(currentMarketIndex));
		}
		boolean hasItemsToOrder = false;

		for (String key : inventory.keySet())
		{
			FoodData ifd = null;
			synchronized(inventory) { ifd = inventory.get(key); }
			FoodData fd = new FoodData(key, ifd.cookTime, ifd.quantity, ifd.low, ifd.high);

			// Theoretical food data object containing the future quantity once all shipments arrive
			synchronized(totals) { fd.quantity = totals.get(key); }
			
			if (markets.size() > 0 && (!checkIfLow || fd.isLow() || hasItemsToOrder))
			{
				// Will always execute if items are already going to be bought
				// Will always execute if the item is low
				// Will always execute if checkIfLow = false
				int difference = fd.high - fd.quantity;
				if (difference > 0)
				{
					mo.addItemToOrder(key, difference);
					hasItemsToOrder = true;
				}
			}

			if (inventory.get(key).quantity <= 0)
			{
				this.host.msgOutOfChoice(key);

				// See if customer must reorder
				ArrayList<Order> matchOrders = new ArrayList<Order>();
				synchronized(orders)
				{
					for (Order o : orders)
					{
						// Find all orders that are ordering the item that is out of stock
						if (o.choice == key && o.status == Status.PENDING)
						{
							matchOrders.add(o);
						}
					}
				}

				if (matchOrders.size() > 0)
				{
					for (Order matchOrder : matchOrders)
					{
						if (matchOrder.choice != null)
						{
							matchOrder.waiter.msgOutOfChoice(matchOrder.table, matchOrder.choice);
							synchronized(orders) { orders.remove(matchOrder); }
							stateChanged();
						}
					}
				} // Otherwise, no customers ordered the out of stock item - nobody needs to reorder
			}
		}

		if (hasItemsToOrder && markets.size() > 0)
		{
			print("ordering more food from market " + markets.get(currentMarketIndex).getName());
			
			// Print out the order items
			TreeMap<String, Integer> orderItems = mo.getOrderItems();
			for (String key : orderItems.keySet())
				print("\t" + orderItems.get(key) + " - " + key);
			
			// Place the order
			markets.get(currentMarketIndex).msgOrderSupplies(mo);
		}

		stateChanged();
	}
	
	private void checkMarketOrder(MarketOrder order)
	{
		boolean orderSufficient = true;
		
		// Add the order quantities to the totals quantity
		ArrayList<MarketItem> predictedShipment = order.getShipmentManifest();
		
		synchronized(totals)
		{
			for (MarketItem mi : predictedShipment)
			{
				String key = mi.getName();
				
				if (totals.containsKey(key))
				{
					totals.put(key, totals.get(key) + mi.getQuantity());
				} else {
					totals.put(key, mi.getQuantity());
				}
				
				
				// Check if the order sufficiently filled the inventory
				if (totals.get(key) < inventory.get(key).high)
				{
					orderSufficient = false;
				}
			}
		}
		
		if (!orderSufficient)
		{
			tryNextMarket = false;
			currentMarketIndex++;
			currentMarketIndex %= markets.size(); // Wrap
			marketTryCount++;
			
			// If all the markets haven't been tried yet, try the next market
			if (marketTryCount < markets.size())
			{
				tryingToFillInventory = true;
			} else {
				marketTryCount = 0;
				tryingToFillInventory = false;
			}
			
		} else {
			marketTryCount = 0; // reset the counter
			tryingToFillInventory = false;
		}		
	}

	private void unpackShipment(ArrayList<MarketItem> shipment)
	{
		System.out.println("Cook is unpacking a shipment...");
		for (MarketItem mi : shipment)
		{
			// Match each with its corresponding FoodData
			for (String key : inventory.keySet())
			{
				if (mi.getName().equals(key))
				{
					FoodData fd = inventory.get(key);

					// Check if need to notify host that an item is back in stock
					if (fd.quantity <= 0 && mi.getQuantity() > 0)
						host.msgItemInStock(key);

					fd.quantity += mi.getQuantity();
					break;
				}	
			}
		}		
	}

	// *** EXTRA -- all the simulation routines***

	/** Returns the name of the cook */
	public String getName(){
		return name;
	}

	private void DoCooking(final Order order){
		print("Cooking:" + order + " for table:" + (order.table+1));
		//put it on the grill. gui stuff
		order.food = new Food(order.choice.substring(0,2),new Color(0,255,255), restaurant);
		order.food.cookFood();

		timer.schedule(new TimerTask(){
			public void run(){//this routine is like a message reception    
				order.status = Status.DONE;
				stateChanged();
			}
		}, (int)(inventory.get(order.choice).cookTime*1000));
	}
	public void DoPlacement(Order order){
		print("Order finished: " + order + " for table:" + (order.table+1));
		order.food.placeOnCounter();
	}


	// Hax
	public void setCashier(Cashier cashier)
	{
		this.cashier = cashier;
	}

	public void setHost(Host host)
	{
		this.host = host;
	}
}



