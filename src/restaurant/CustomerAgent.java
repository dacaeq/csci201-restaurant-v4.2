package restaurant;

import restaurant.gui.RestaurantGui;
import restaurant.interfaces.Cashier;
import restaurant.interfaces.Customer;
import restaurant.interfaces.Host;
import restaurant.interfaces.Waiter;
import restaurant.layoutGUI.*;
import agent.Agent;
import java.util.*;
import java.awt.Color;

/** Restaurant customer agent. 
 * Comes to the restaurant when he/she becomes hungry.
 * Randomly chooses a menu item and simulates eating 
 * when the food arrives. 
 * Interacts with a waiter only */
public class CustomerAgent extends Agent implements Customer
{
	private String name;
	private int hungerLevel = 5;  // Determines length of meal
	private int cash = 10;
//	private Food food;
	private Menu menu;
	private CustomerBill bill;
	private Receipt recept;
	private int punishmentDuration = -1;
	private RestaurantGui gui;

	// ** Agent connections **
	private Host host;
	private Waiter waiter;
	private Cashier cashier;
	Restaurant restaurant;
	Timer timer = new Timer();
	GuiCustomer guiCustomer; //for gui
	
	// ** Agent state **
	private boolean isHungry = false; //hack for gui
	
	public enum AgentState {DoingNothing, ON_WAITLIST, WaitingInRestaurant, SeatedWithMenu, WaiterCalled,
							WaitingForFood, REORDERING, Eating, WAITING_FOR_BILL, PAYING, WASHING_DISHES};
							
	private AgentState state = AgentState.DoingNothing; //The start state
	
	public enum AgentEvent {gotHungry, DECIDING_WAITLIST, beingSeated, decidedChoice, waiterToTakeOrder,
							foodDelivered, doneEating, GOT_BILL, GOT_RECEPT, CHOICE_IS_OUT, WASH_DISHES, DONE_WASHING};
							
	private List<AgentEvent> events = new ArrayList<AgentEvent>();
	
//	private Semaphore customerSem = new Semaphore(1);
	

	/** Constructor for CustomerAgent class 
	 * @param name name of the customer
	 * @param gui reference to the gui so the customer can send it messages
	 */
	public CustomerAgent(String name, RestaurantGui gui, Restaurant restaurant) {
		super();
		this.gui = gui;
		this.name = name;
		cash = (int) Math.round(Math.random()*30);
		this.restaurant = restaurant;
		guiCustomer = new GuiCustomer(name.substring(0,2), new Color(0,255,0), restaurant);
	}
	public CustomerAgent(String name, Restaurant restaurant) {
		super();
		this.gui = null;
		this.name = name;
		cash = (int) Math.round(Math.random()*30);
		this.restaurant = restaurant;
		guiCustomer = new GuiCustomer(name.substring(0,1), new Color(0,255,0), restaurant);
	}
	
	public int getCash()
	{
		return cash;
	}
	
	public void setCash(int cash)
	{
		this.cash = cash;
	}
	
	
	//BOOKMARK
	// *** MESSAGES ***
	
	/** Sent from GUI to set the customer as hungry */
	public void setHungry() {
		events.add(AgentEvent.gotHungry);
		isHungry = true;
		print("I'm hungry");
		stateChanged();
	}
	
	public void msgThereIsAWait()
	{
		events.add(AgentEvent.DECIDING_WAITLIST);
		stateChanged();
	}
	
	/** Waiter sends this message so the customer knows to sit down 
	 * @param waiter the waiter that sent the message
	 * @param menu a reference to a menu */
	public void msgFollowMeToTable(Waiter waiter, Menu menu) {
		this.menu = menu.deepCopy();
		this.waiter = waiter;
		print("Received msgFollowMeToTable from" + waiter);
		events.add(AgentEvent.beingSeated);
		stateChanged();
	}
	
	/** Waiter sends this message to take the customer's order */
	public void msgDecided(){
		events.add(AgentEvent.decidedChoice);
		stateChanged(); 
	}
	
	/** Waiter sends this message to take the customer's order */
	public void msgWhatWouldYouLike(){
		events.add(AgentEvent.waiterToTakeOrder);
		stateChanged(); 
	}

	/** Waiter sends this when the food is ready 
	 * @param choice the food that is done cooking for the customer to eat */
	public void msgHereIsYourFood(String choice) {
		events.add(AgentEvent.foodDelivered);
		stateChanged();
	}
	/** Timer sends this when the customer has finished eating */
	public void msgDoneEating() {
		events.add(AgentEvent.doneEating);
		stateChanged(); 
	}
	
	/** Sent from waiter */
	public void msgHereIsBill(CustomerBill bill)
	{		
		this.bill = bill;
		events.add(AgentEvent.GOT_BILL);
		stateChanged();
	}
	
	/** Sent from cashier if the customer paid in full */
	public void msgThankYouForComing(Receipt recept)
	{
		this.recept = recept;
		events.add(AgentEvent.GOT_RECEPT);
		stateChanged();
	}
	
	/** Sent from waiter if choice is out of stock */
	public void msgPleaseReorder(Menu updatedMenu)
	{
		this.menu = updatedMenu;
		events.add(AgentEvent.CHOICE_IS_OUT);
		stateChanged();
	}
	
	/** Sent by cashier if the customer couldn't pay */
	public void msgWashDishes(int duration)
	{
		events.add(AgentEvent.WASH_DISHES);
		this.punishmentDuration = duration;
		stateChanged();
	}


	//BOOKMARK
	/** Scheduler.  Determine what action is called for, and do it. */
	protected boolean pickAndExecuteAnAction() {
		if (events.isEmpty()) return false;
		AgentEvent event = events.remove(0); //pop first element
		if (event == null)
			return false;

		//Simple finite state machine
		if (state == AgentState.DoingNothing){
			if (event == AgentEvent.gotHungry)	{
				goingToRestaurant();
				state = AgentState.WaitingInRestaurant;
				return true;
			}
			// elseif (event == xxx) {}
		}

		if (state == AgentState.WaitingInRestaurant || state == AgentState.ON_WAITLIST) {
			if (event == AgentEvent.beingSeated)	{
				makeMenuChoice();
				state = AgentState.SeatedWithMenu;
				return true;
			}
			if (event == AgentEvent.DECIDING_WAITLIST)
			{
				decideToWait();
				return false;
			}
		}

		if (state == AgentState.SeatedWithMenu) {
			if (event == AgentEvent.decidedChoice)	{
				callWaiter();
				state = AgentState.WaiterCalled;
				return true;
			}
		}
		if (state == AgentState.WaiterCalled) {
			if (event == AgentEvent.waiterToTakeOrder)	{
				orderFood();
				state = AgentState.WaitingForFood;
				return true;
			}
		}
		if (state == AgentState.WaitingForFood) {
			if (event == AgentEvent.foodDelivered)	{
				eatFood();
				state = AgentState.Eating;
				return true;
			}
			if (event == AgentEvent.CHOICE_IS_OUT)
			{
				System.out.println("Customer " + name + " is reordering...");
				state = AgentState.REORDERING;
				makeMenuChoice();
				return true;
			}
		}

		if (state == AgentState.REORDERING)
		{
			if (event == AgentEvent.decidedChoice)
			{
				callWaiter();
				state = AgentState.WaiterCalled;
				return true;
			}
		}
		if (state == AgentState.Eating) {
			if (event == AgentEvent.doneEating)	{
				requestBill();
				state = AgentState.WAITING_FOR_BILL;
				return true;
			}
		}
		if (state == AgentState.WAITING_FOR_BILL) {
			if (event == AgentEvent.GOT_BILL)	{
				payBill();
				state = AgentState.PAYING;
				return true;
			}
		}
		if (state == AgentState.PAYING) {
			if (event == AgentEvent.GOT_RECEPT)	{
				getChange();
				leaveRestaurant();
				state = AgentState.DoingNothing;
				return true;
			}
			
			if (event == AgentEvent.WASH_DISHES)	{
				washDishes();
				state = AgentState.WASHING_DISHES;
				return true;
			}
		}
		
		if (state == AgentState.WASHING_DISHES)
		{
			if (event == AgentEvent.DONE_WASHING)
			{
				leaveRestaurant();
				return true;
			}
		}
		

		print("No scheduler rule fired, should not happen in FSM, event="+event+" state="+state);
		return false;
	}

	
	//BOOKMARK
	// *** ACTIONS ***

	/** Goes to the restaurant when the customer becomes hungry */
	private void goingToRestaurant() {
		print("Going to restaurant");
		guiCustomer.appearInWaitingQueue();
		host.msgIWantToEat(this);//send him our instance, so he can respond to us
		stateChanged();
	}
	
	private void decideToWait()
	{
		if (Math.random() < 0.5)
		{
			// Stay (do nothing - go into waiting until a waiter asks you to follow him)
			state = AgentState.ON_WAITLIST;
		} else {
			System.out.println("Customer " + name + " decided to leave instead of wait for a table.");
			host.msgImLeaving(this);
			leaveRestaurant();
		}
	}

	/** Starts a timer to simulate the customer thinking about the menu */
	private void makeMenuChoice(){
		print("Deciding menu choice...(3000 milliseconds)");
		timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {  
				msgDecided();	    
			}},
			3000);//how long to wait before running task
		stateChanged();
	}
	private void callWaiter(){
		print("I decided!");
		waiter.msgImReadyToOrder(this);
		stateChanged();
	}

	/** Picks a random choice from the menu and sends it to the waiter */
	private void orderFood()
	{
		Object[] menuChoices = menu.getChoices().toArray();

		// Only order items you can afford
		String choice = null;
		boolean choosing = true;
		while (choosing)
		{
			choice = (String)menuChoices[(int)(Math.random()*menuChoices.length)];
			if (menu.getPriceOf(choice) <= this.cash)
			{
				// Can afford it
				choosing = false;
				break;
			} else {
				// Forget you don't have enough money 20% of the time
				if (Math.random() < 0.2)
				{
					choosing = false;
					break;
				}
			}
		}

		print("Ordering the " + choice);
		waiter.msgHereIsMyChoice(this, choice);
		stateChanged();
	}

	/** Starts a timer to simulate eating */
	private void eatFood() {
		print("Eating for " + hungerLevel*1000 + " milliseconds.");
		timer.schedule(new TimerTask() {
			public void run() {
				msgDoneEating();    
			}},
			getHungerLevel() * 1000);//how long to wait before running task

		stateChanged();
	}
	
	private void requestBill()
	{
		waiter.msgReadyForBill(this);
		stateChanged();
	}
	
	private void payBill()
	{	
		if (cash >= bill.checkCharge())
		{
			// Can pay
			cash = bill.pay(cash, bill.checkCharge());
		} else {
			System.out.println("Customer " + this.name + " could not pay the bill!");
			bill.state = Bill.BillState.UNPAID;
		}
		
		cashier.msgHereIsBill(bill);
		stateChanged();
	}
	
	private void getChange()
	{
		cash += recept.takeChange();
		stateChanged();
	}
	
	private void washDishes()
	{
		System.out.println("Customer " + this.name + " is going to wash dishes for " + punishmentDuration + " seconds.");
		// Just use thread sleep to simulate punishment
		try {
			Thread.sleep(punishmentDuration*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.events.add(AgentEvent.DONE_WASHING);
		System.out.println("Customer " + this.name + " is done washing dishes.");
		stateChanged();
	}

	/** When the customer is done eating, he leaves the restaurant */
	private void leaveRestaurant() {
		// Waiter might be null if the customer leaves before being seated
		if (waiter != null)
			waiter.msgImLeaving(this);
		print("Leaving the restaurant");
		guiCustomer.leave(); //for the animation
		isHungry = false;
		stateChanged();
		gui.setCustomerEnabled(this); //Message to gui to enable hunger button

		//hack to keep customer getting hungry. Only for non-gui customers
		if (gui==null) becomeHungryInAWhile();//set a timer to make us hungry.
	}

	/** This starts a timer so the customer will become hungry again.
	 * This is a hack that is used when the GUI is not being used */
	private void becomeHungryInAWhile() {
		timer.schedule(new TimerTask() {
			public void run() {  
				setHungry();		    
			}},
			15000);//how long to wait before running task
	}

	// *** EXTRA ***

	/** establish connection to host agent. 
	 * @param host reference to the host */
	public void setHost(Host host) {
		this.host = host;
	}
	
	public void setCashier(Cashier cashier)
	{
		this.cashier = cashier;
	}

	/** Returns the customer's name
	 *@return name of customer */
	public String getName() {
		return name;
	}

	/** @return true if the customer is hungry, false otherwise.
	 ** Customer is hungry from time he is created (or button is
	 ** pushed, until he eats and leaves.*/
	public boolean isHungry() {
		return isHungry;
	}

	/** @return the hungerlevel of the customer */
	public int getHungerLevel() {
		return hungerLevel;
	}

	/** Sets the customer's hungerlevel to a new value
	 * @param hungerLevel the new hungerlevel for the customer */
	public void setHungerLevel(int hungerLevel) {
		this.hungerLevel = hungerLevel; 
	}
	public GuiCustomer getGuiCustomer(){
		return guiCustomer;
	}

	/** @return the string representation of the class */
	public String toString() {
		return "customer " + getName();
	}


}

