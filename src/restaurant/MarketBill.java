package restaurant;

import restaurant.interfaces.Market;

/**
 * 
 * @author Clayton Ketner
 *
 */
public class MarketBill extends Bill
{
	public Market market;
	
	public MarketBill(Market market)
	{
		super();
		this.market = market;
	}
}
