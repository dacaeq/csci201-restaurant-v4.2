# README
# v4.2 Requirements
## Part 4 � Make sure your agents are thread-safe. This means:
 
Synchronizing your lists using Collections.synchronizedList (), e.g.
                         
private List<CustomerAgent> waitList = Collections.synchronizedList(new ArrayList<CustomerAgent>());

Putting synchronized blocks around list searches: e.g.
synchronized(waitList){ tellWaiterToSitCustomerAtTable(waiters.get(nextWaiter), waitList.get(0), i); }
 
Both of the above examples are from the HostAgent.
 
## Part 5 - Implement multi-step actions.                                                                                                                          
Use this Waiter-Customer ordering scenario as in the original v3 interaction diagram:

* Waiter seats customer and �leaves.�
* Customer tells waiter he is ready to order.
* Waiter asks customer what he wants.
* Customer tells him.
* Waiter gives the cook the order (as before).
 
Steps 2-5 should be carried out via one action in the customer and one action in the waiter.
 
## Part 6 - Incorporate Shared Data between Cook and Waiter.
                                   
In v3.0, the cook receives orders directly from waiters. We are going to change that interaction. Here are the new requirements:

* Some waiters will NOT send the �HereIsOrder() messages to the cook.
* Instead, those waiters will put orders in one of those �revolving stands�.
* The cooks will inform those waiters just like they always do when an order is ready.
* Some waiters will work the same as they did in V3.
 
You will do this with shared data by incorporating the Producer-Consumer code on the class website at:
ProducerConsumerMonitor.txt

Here are the key issues to keep in mind:

* Which waiters will be like in V3 and which will be shared-data waiters?
* Who should create the shared data?
* How are cooks and waiters made aware of the shared data?
* The cook must have a way to wake up and look at the shared data.
                               
## Part 7 - Unit Test your Cashier.

Each student will unit test their own v4.2 Cashier. You are to design the tests and document them as shown below. You will implement some number of the tests so that you are confident that it really works. Of course, this is in preparation for the major team project.